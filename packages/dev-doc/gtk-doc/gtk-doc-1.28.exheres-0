# Copyright 2008 Alexander Færøy <ahf@exherbo.org>
# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] python [ blacklist=none multibuild=false ]

SUMMARY="GTK API documentation generator"
HOMEPAGE="https://www.gtk.org/${PN}"

UPSTEAM_DOCUMENTATION="
    https://developer.gnome.org/gtk-doc-manual/stable/ [[
        description = [ User manual ]
    ]]
"

LICENCES="FDL-1.1 GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    source-highlight [[ description = [ Support source highlighting using source-highlight ] ]]
    vim [[ description = [ Support source highlighting using vim ] ]]

    ( source-highlight vim ) [[ number-selected = at-most-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.19]
    build+run:
        app-text/docbook-xml-dtd:4.3
        app-text/docbook-xsl-stylesheets
        dev-libs/libxml2:2.0[>=2.3.6]
        dev-libs/libxslt
        gnome-desktop/yelp-tools
        source-highlight? ( app-text/source-highlight )
        vim? ( app-editors/vim )
    run:
        app-doc/gtk-doc-autotools[~${PV}][python_abis:*(-)?]
    test:
        dev-libs/glib:2[>=2.6]
        x11-libs/gtk+:3
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-highlight=no
    PYTHON=${PYTHON}
)
DEFAULT_SRC_CONFIGURE_OPTIONS=(
    'source-highlight --with-hightlighter=source-highlight'
    'vim --with-highlighter=vim'
)

DEFAULT_SRC_TEST_PARAMS=( -j1 V=1 )

src_install() {
    default

    # These are now provided by the gtk-doc-autotools package
    edo rm -r "${IMAGE}"usr/$(exhost --target)/bin/{gtkdoc-rebase,gtkdocize} \
           "${IMAGE}"usr/share/aclocal/gtk-doc.m4      \
           "${IMAGE}"usr/share/gtk-doc/data/gtk-doc{,.flat}.make \
           "${IMAGE}"usr/share/gtk-doc/python

    edo rmdir "${IMAGE}"usr/share/aclocal
}

