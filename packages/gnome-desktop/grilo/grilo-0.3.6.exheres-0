# Copyright 2013 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require vala [ vala_dep=true with_opt=true option_name=vapi ]
require option-renames [ renames=[ 'vala vapi' ] ]
require meson

SUMMARY="A framework that provides access to various sources of multimedia content, using a
pluggable system"

LICENCES="LGPL-2.1"
SLOT="0.3"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk [[ description = [ Build the grilo-test-ui tool ] ]]
    gtk-doc
    vapi [[ requires = gobject-introspection ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
        gobject-introspection? ( gnome-desktop/gobject-introspection[>=0.9] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.10] )
    build+run:
        dev-libs/glib:2[>=2.44]
        dev-libs/libxml2:2.0
        gnome-desktop/libsoup:2.4[>=2.41.3]
        gnome-desktop/totem-pl-parser[>=3.4.1]
        gtk? (
            dev-libs/liboauth [[ note = automagic ]]
            x11-libs/gtk+:3[>=3.0.0]
        )
    post:
        gnome-desktop/grilo-plugins:0.3
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Denable-grl-net=true
    -Denable-grl-pls=true
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection enable-introspection'
    'gtk enable-test-ui'
    'gtk-doc enable-gtk-doc'
    'vapi enable-vala'
)

